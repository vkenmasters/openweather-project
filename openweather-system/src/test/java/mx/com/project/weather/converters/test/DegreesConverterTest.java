package mx.com.project.weather.converters.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import mx.com.project.weather.converters.DegreesConverter;
import mx.com.project.weather.exception.WeatherException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/META-INF/spring/application-context.xml"})
public class DegreesConverterTest {

	@Autowired
	DegreesConverter degreesConverter;

	@Test
	public void convertCelsiusToFahrenheitTest() {
		String celsiusDegrees = "87";
		assertEquals(degreesConverter.convertCelsiusToFahrenheit(celsiusDegrees), "188.60");
	}

	@Test(expected = WeatherException.class)
	public void convertCelsiusToFahrenheitTestNotNumber() {
		String celsiusDegrees = "NOT_A_NUMBER";
		degreesConverter.convertCelsiusToFahrenheit(celsiusDegrees);
	}

}
