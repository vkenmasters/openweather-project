package mx.com.project.weather.callers.test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import mx.com.project.weather.callers.OpenWeatherServiceCaller;
import mx.com.project.weather.exception.WeatherException;
import mx.com.project.weather.request.RequestCityWeather;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/META-INF/spring/application-context.xml"})
public class OpenWeatherServiceCallerTest {

	@Autowired
	OpenWeatherServiceCaller caller;
	
	@Mock
	RestTemplate restTemplate;
	
	@Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

	@Test
	public void callOpenWeatherServiceOkTest() {
		String url = "http://api.openweathermap.org/data/2.5/weather?q=London&appid=bc640d2ffd449ae5f55eff67b7984336&mode=xml&units=metric";
		Mockito.when(restTemplate.postForEntity(url, null ,String.class ))
		.thenReturn(ResponseEntity.ok(""));
		
		RequestCityWeather request = new RequestCityWeather();
		request.setCityName("London");
		ResponseEntity<String> response = caller.callOpenWeatherService(request);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		
		
//		givenThat(get(urlEqualTo("/data/2.5/weather?q=Londor&appid=bc640d2ffd449ae5f55eff67b7984336&mode=xml&units=metric"))
//				.willReturn(aResponse()
//				.withStatus(200)));
//		
//		RequestCityWeather request = new RequestCityWeather();
//		request.setCityName("Londor");
//		ResponseEntity<String> response = caller.callOpenWeatherService(request);
//		
//		assertEquals(HttpStatus.OK, response.getStatusCode());*/
//		givenThat(get(urlEqualTo("/baeldung")).willReturn(aResponse().withBody("Welcome to Baeldung!")));
//		CloseableHttpClient httpClient = HttpClients.createDefault();
//		HttpGet request = new HttpGet("http://localhost:8080/baeldung");
//		try {
//			HttpResponse httpResponse = httpClient.execute(request);
//			String responseString = convertResponseToString(httpResponse);
//			System.out.println("DAMN: " + responseString);
//		} catch (ClientProtocolException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}

	@Test(expected = WeatherException.class)
	public void callOpenWeatherServiceErrorTest() {
		String url = "http://api.openweathermap.org/data/2.5/weather?q=London&appid=bc640d2ffd449ae5f55eff67b7984336&mode=xml&units=metric";
		Mockito.when(restTemplate.postForEntity(url, null ,String.class ))
		.thenReturn(ResponseEntity.ok(""));
		
		RequestCityWeather request = new RequestCityWeather();
		request.setCityName("Londor");
		caller.callOpenWeatherService(request);
	}

}
