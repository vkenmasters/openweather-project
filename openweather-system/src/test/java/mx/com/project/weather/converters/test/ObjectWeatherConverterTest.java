package mx.com.project.weather.converters.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import mx.com.project.weather.converters.ObjectWeatherConverter;
import mx.com.project.weather.domain.CityWeather;
import mx.com.project.weather.exception.WeatherException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/META-INF/spring/application-context.xml"})
public class ObjectWeatherConverterTest {

	@Autowired
	ObjectWeatherConverter objectWeatherConverter;

	@Mock
	String responseXml;
	
	private String generateValidXml() {
		return "<current><city id=\"2643743\" "
				+ "name=\"London\"><coord lon=\"-0.13\" lat=\"51.51\"/><country>GB"
				+ "</country><sun rise=\"2019-06-29T03:46:02\" "
				+ "set=\"2019-06-29T20:21:32\"/></city><temperature "
				+ "value=\"287.32\" min=\"284.82\" max=\"289.26\" "
				+ "unit=\"kelvin\"/><humidity value=\"87\" unit=\"%\"/>"
				+ "<pressure value=\"1021\" unit=\"hPa\"/><wind>"
				+ "<speed value=\"2.1\" name=\"Light breeze\"/><gusts/>"
				+ "<direction value=\"50\" code=\"NE\" name=\"NorthEast\"/>"
				+ "</wind><clouds value=\"11\" name=\"few clouds\"/>"
				+ "<visibility value=\"8000\"/><precipitation mode=\"no\"/>"
				+ "<weather number=\"701\" value=\"mist\" icon=\"50n\"/>"
				+ "<lastupdate value=\"2019-06-29T00:56:26\"/></current>";
	}

	@Test
	public void convertXMLToCityWeatherObjectTest() {
		responseXml = generateValidXml();
		CityWeather result = objectWeatherConverter.convertXMLToCityWeatherObject(responseXml);
		assertEquals(result.getCityName(), "London");
	}

	@Test(expected = WeatherException.class)
	public void convertXMLToCityWeatherObjectTestBadXml() {
		responseXml = "<bad_xml></bad_xml>";
		objectWeatherConverter.convertXMLToCityWeatherObject(responseXml);
	}
	

}
