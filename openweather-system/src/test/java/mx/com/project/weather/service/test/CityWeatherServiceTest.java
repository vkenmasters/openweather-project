package mx.com.project.weather.service.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import mx.com.project.weather.callers.OpenWeatherServiceCaller;
import mx.com.project.weather.request.RequestCityWeather;
import mx.com.project.weather.response.ResponseCityWeather;
import mx.com.project.weather.service.CityWeatherService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/META-INF/spring/application-context.xml"})
public class CityWeatherServiceTest {

	@InjectMocks
	@Autowired
	CityWeatherService cityWeatherService;

	@Mock
	private OpenWeatherServiceCaller openWeatherServiceCaller;

	@Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

	private ResponseEntity<String> generateValidResponse() {
		return new ResponseEntity<String>("<current><city id=\"2643743\" "
				+ "name=\"London\"><coord lon=\"-0.13\" lat=\"51.51\"/>"
				+ "<country>GB</country><sun rise=\"2019-06-29T03:46:02\" "
				+ "set=\"2019-06-29T20:21:32\"/></city><temperature "
				+ "value=\"14.13\" min=\"11.67\" max=\"16.67\" "
				+ "unit=\"celsius\"/><humidity value=\"87\" "
				+ "unit=\"%\"/><pressure value=\"1021\" "
				+ "unit=\"hPa\"/><wind><speed value=\"2.6\" "
				+ "name=\"Light breeze\"/><gusts/><direction value=\"60\" "
				+ "code=\"ENE\" name=\"East-northeast\"/></wind><clouds "
				+ "value=\"11\" name=\"few clouds\"/><visibility value=\"8000\"/>"
				+ "<precipitation mode=\"no\"/><weather number=\"741\" "
				+ "value=\"fog\" icon=\"50n\"/><lastupdate "
				+ "value=\"2019-06-29T01:17:15\"/></current>",HttpStatus.OK);
	}

	private ResponseEntity<String> generateWrongResponse() {
		return new ResponseEntity<String>("<ClientError><cod>404</cod>"
				+ "<message>city not found</message></ClientError>",HttpStatus.NOT_FOUND);
	}

	@Test
	public void getCityWeatherTest() {
		RequestCityWeather request = new RequestCityWeather();
		request.setCityName("London");
		Mockito.when(openWeatherServiceCaller.callOpenWeatherService(request))
		.thenReturn(generateValidResponse());

		ResponseCityWeather weather = cityWeatherService.getCityWeather(request);
		assertNotNull(weather);
		assertEquals(weather.getResultFound(),true);
		assertEquals(weather.getErrorMessage(), null);
		assertNotNull(weather.getResult());
		assertEquals(weather.getResult().getCityName(), "London");
	}

	@Test
	public void getCityWeatherTestNotResponse() {
		RequestCityWeather request = new RequestCityWeather();
		request.setCityName("Londor");
		Mockito.when(openWeatherServiceCaller.callOpenWeatherService(request))
		.thenReturn(generateWrongResponse());

		ResponseCityWeather weather = cityWeatherService.getCityWeather(request);
		assertNotNull(weather);
		assertEquals(weather.getResultFound(),false);
		assertNotNull(weather.getErrorMessage());
		assertNull(weather.getResult());
	}
}
