package mx.com.project.weather.converters;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import mx.com.project.weather.exception.WeatherException;

@Service("degreesConverter")
public class DegreesConverter {

	private final static Log log = LogFactory.getLog(DegreesConverter.class);
	private final float fahrenheitMultiplier = 1.8f;
	private final int fahrenheitFactor = 32;

	/**
	* Converts Celsius Degrees to Fahrenheit Degrees
	*
	* @author  Salvador Gonzalez
	* @version 1.0
	* @since   2019-06-18 
	*/
	public String convertCelsiusToFahrenheit(String celciusDegrees) throws WeatherException{
		
		BigDecimal fahrenheitDegrees = null;
		String errorMessage = "";
		
		try {
			fahrenheitDegrees = new BigDecimal(celciusDegrees)
								.multiply(new BigDecimal(fahrenheitMultiplier))
								.add(new BigDecimal(fahrenheitFactor))
								.setScale(2, RoundingMode.HALF_UP);
		}catch(Exception ex) {
			errorMessage = "An error ocurred while converting the degrees";
			log.error(errorMessage,ex);
			throw new WeatherException(errorMessage,ex);
		}
		return fahrenheitDegrees.toString();
	}

}
