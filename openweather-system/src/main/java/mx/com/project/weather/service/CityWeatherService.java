package mx.com.project.weather.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import mx.com.project.weather.callers.OpenWeatherServiceCaller;
import mx.com.project.weather.converters.DegreesConverter;
import mx.com.project.weather.converters.ObjectWeatherConverter;
import mx.com.project.weather.domain.CityWeather;
import mx.com.project.weather.exception.WeatherException;
import mx.com.project.weather.request.RequestCityWeather;
import mx.com.project.weather.response.ResponseCityWeather;

@Service
public class CityWeatherService {

	private final static Log log = LogFactory.getLog(CityWeatherService.class);

	@Autowired
	private DegreesConverter degreesConverter;
	@Autowired
	private OpenWeatherServiceCaller openWeatherServiceCaller;
	@Autowired
	private ObjectWeatherConverter objectWeatherConverter;

	public ResponseCityWeather getCityWeather(RequestCityWeather request) {
		ResponseCityWeather response = new ResponseCityWeather();
		String errorMessage = "";
		CityWeather requiredWeather = null;
		
		try {
			ResponseEntity<String> responseFromApi = openWeatherServiceCaller
												 .callOpenWeatherService(request);
			
			if(responseFromApi.getStatusCode() == HttpStatus.OK) {
				requiredWeather = objectWeatherConverter
								  .convertXMLToCityWeatherObject(responseFromApi.getBody());
				requiredWeather.setFahrenheitDegrees(degreesConverter
								.convertCelsiusToFahrenheit(
										requiredWeather.getCelsiusDegrees()));
				response.setResultFound(true);
				response.setResult(requiredWeather);
			}else {
				errorMessage = "The weather api has return an unexpected status. Please contact the administrator";
				log.error(errorMessage);
				response.setResultFound(false);
				response.setResult(null);
				response.setErrorMessage(errorMessage);
			}
		}catch(WeatherException e) {
			log.error(e);
			response.setResultFound(false);
			response.setResult(null);
			response.setErrorMessage(e.getMessage());			
		}
		return response;
	}
}
