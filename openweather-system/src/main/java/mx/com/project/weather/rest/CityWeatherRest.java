package mx.com.project.weather.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.project.weather.request.RequestCityWeather;
import mx.com.project.weather.response.ResponseCityWeather;
import mx.com.project.weather.service.CityWeatherService;

@Consumes( MediaType.APPLICATION_JSON )
@Produces( MediaType.APPLICATION_JSON )
@Service("cityWeatherRest")
public class CityWeatherRest {
	
	public static final String GET_CITY_WEATHER = "/weather/request";

	@Autowired
	private CityWeatherService cityWeatherService;

	@POST
	@Path( GET_CITY_WEATHER )
	public ResponseCityWeather getCityWeather(RequestCityWeather request) {
		return cityWeatherService.getCityWeather(request);
	}

}
