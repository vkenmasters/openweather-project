package mx.com.project.weather.request;

/**
* The container event for the request parameters
*
* @author  Salvador Gonzalez
* @version 1.0
* @since   2019-06-18 
*/
public class RequestCityWeather {

	private String cityName;

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

}
