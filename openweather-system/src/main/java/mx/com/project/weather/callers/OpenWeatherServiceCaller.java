package mx.com.project.weather.callers;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import mx.com.project.weather.exception.WeatherException;
import mx.com.project.weather.request.RequestCityWeather;

@Service("openWeatherServiceCaller")
public class OpenWeatherServiceCaller {

	private final static Log log = LogFactory.getLog(OpenWeatherServiceCaller.class);
	@Value("${weather.apiKey}")
	private String apiKey;
	@Value("${weather.url}")
	private String weatherUrl;	
	
	@Bean
	public RestTemplate getRestTemplate(){
		return new RestTemplate();
	}

	/**
	* Invokes the API from Open Weather
	*
	* @author  Salvador Gonzalez
	* @version 1.0
	* @since   2019-06-18 
	*/
	public ResponseEntity<String> callOpenWeatherService(RequestCityWeather request) throws WeatherException{
		ResponseEntity<String> response = null;
		String errorMessage = "";
		try {
			StringBuilder url = new StringBuilder(weatherUrl);
			url.append("?q=");
			url.append(request.getCityName());
			url.append("&APPID=");
			url.append(apiKey);
			url.append("&mode=xml");
			url.append("&units=metric");
			
			response = getRestTemplate().postForEntity( 
								url.toString(), null , String.class );
		}catch(RestClientException ex) {
			errorMessage = "There's been an error calling the Open Weather API";
			log.error(errorMessage, ex);
			throw new WeatherException(errorMessage, ex); 
		}
		return response;
	}

}
