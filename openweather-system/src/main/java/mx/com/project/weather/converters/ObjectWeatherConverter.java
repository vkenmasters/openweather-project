package mx.com.project.weather.converters;

import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import mx.com.project.weather.domain.CityWeather;
import mx.com.project.weather.exception.WeatherException;

@Service("objectWeatherConverter")
public class ObjectWeatherConverter {

	private final static Log log = LogFactory.getLog(ObjectWeatherConverter.class);

	public CityWeather convertXMLToCityWeatherObject(String xml) throws WeatherException{

		CityWeather resultCityWeather = null;
		String errorMessage = "";

		try {
			resultCityWeather = new CityWeather();
			StringBuilder weatherDescription = new StringBuilder();
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance(); 
			DocumentBuilder builder = factory.newDocumentBuilder();   

			Document document = builder.parse( new InputSource(   
					new StringReader( xml ) ) );
			Element formattedXml =  (Element)document.getChildNodes().item(0);
			NodeList nList = formattedXml.getChildNodes();
			
			Node nNode = nList.item(0);
			Element eElement = (Element) nNode;
			resultCityWeather.setCityName(eElement.getAttribute("name"));
			Element sun = (Element) eElement.getChildNodes().item(2);
			SimpleDateFormat fullDateFormat  = new SimpleDateFormat("YYYY-MM-dd'T'HH:mm:ss");
			Date sunriseTime = fullDateFormat.parse(sun.getAttribute("rise"));
			Date sunsetTime = fullDateFormat.parse(sun.getAttribute("set"));							
			SimpleDateFormat twelveHourFormat = new SimpleDateFormat("hh:mm a");
			resultCityWeather.setSunriseTime(twelveHourFormat.format(sunriseTime));
			resultCityWeather.setSunsetTime(twelveHourFormat.format(sunsetTime));
			
			nNode = nList.item(1);
			eElement = (Element) nNode;
			resultCityWeather.setCelsiusDegrees(eElement.getAttribute("value"));
			
			nNode = nList.item(4);
			eElement = (Element) nNode;
			Element wind = (Element) eElement.getChildNodes().item(0);
			weatherDescription.append(wind.getAttribute("name"));
			weatherDescription.append(", ");
			
			nNode = nList.item(5);
			eElement = (Element) nNode;
			weatherDescription.append(eElement.getAttribute("name"));
	
			resultCityWeather.setOverallDescription(weatherDescription.toString());
		}catch(ParseException | ParserConfigurationException | SAXException | IOException ex) {
			errorMessage = "There's been an error setting the dates";
			log.error(errorMessage, ex);
			throw new WeatherException(errorMessage, ex); 
		}
		
		return resultCityWeather;
	}

}
