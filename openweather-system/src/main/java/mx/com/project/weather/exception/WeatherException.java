package mx.com.project.weather.exception;

public class WeatherException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5831767521597291071L;

	public WeatherException(Throwable cause) {
		super(cause);
	}

	public WeatherException(String message, Throwable cause) {
		super(message, cause);
	}

	public WeatherException(String message) {
		super(message);
	}

	public WeatherException() {
		super();
	}
}
