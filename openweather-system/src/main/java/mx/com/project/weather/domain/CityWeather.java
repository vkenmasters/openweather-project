package mx.com.project.weather.domain;

/**
* The POJO that contains all the information needed
* for displaying the weather of a City
*
* @author  Salvador Gonzalez
* @version 1.0
* @since   2019-06-18 
*/
public class CityWeather {

	private String todayDate;
	private String cityName;
	private String overallDescription;
	private String fahrenheitDegrees;
	private String celsiusDegrees;
	private String sunriseTime;
	private String sunsetTime;

	public String getTodayDate() {
		return todayDate;
	}

	public void setTodayDate(String todayDate) {
		this.todayDate = todayDate;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getOverallDescription() {
		return overallDescription;
	}

	public void setOverallDescription(String overallDescription) {
		this.overallDescription = overallDescription;
	}

	public String getFahrenheitDegrees() {
		return fahrenheitDegrees;
	}

	public void setFahrenheitDegrees(String fahrenheitDegrees) {
		this.fahrenheitDegrees = fahrenheitDegrees;
	}

	public String getCelsiusDegrees() {
		return celsiusDegrees;
	}

	public void setCelsiusDegrees(String celsiusDegrees) {
		this.celsiusDegrees = celsiusDegrees;
	}

	public String getSunriseTime() {
		return sunriseTime;
	}

	public void setSunriseTime(String sunriseTime) {
		this.sunriseTime = sunriseTime;
	}

	public String getSunsetTime() {
		return sunsetTime;
	}

	public void setSunsetTime(String sunsetTime) {
		this.sunsetTime = sunsetTime;
	}

}
