package mx.com.project.weather.response;

import mx.com.project.weather.domain.CityWeather;

/**
* The request container for the search's result
*
* @author  Salvador Gonzalez
* @version 1.0
* @since   2019-06-18 
*/
public class ResponseCityWeather {

	private Boolean resultFound;
	private String errorMessage;
	private CityWeather result;

	public Boolean getResultFound() {
		return resultFound;
	}

	public void setResultFound(Boolean resultFound) {
		this.resultFound = resultFound;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public CityWeather getResult() {
		return result;
	}

	public void setResult(CityWeather result) {
		this.result = result;
	}

}
