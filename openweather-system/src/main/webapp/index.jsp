<html>
<head>
<script src="javascript/vendor/jquery-3.4.1.min.js"></script>
<style type="text/css">
label{
	font-weight : bold
}
</style>
<script>
$(document).ready(function(){
	  $("#city").change(function(){
		  if($(this).val() != ""){
			  $.ajax({
			        type 		:   "POST",
			        contentType: "application/json",
			    	dataType	:   "json",   
			    	url			:   "http://localhost:8080/weather-system/services/weather/request",
			    	data: JSON.stringify(eval({ 
			    		"cityName": $("#city").val()
			        })),   
			    	success		:   function (data) {
			    		if(data.resultFound == true){
			    			$("#cityName").html(data.result.cityName);
			    			$("#todayDate").html(data.result.todayDate);
			    			$("#celsius").html(data.result.celsiusDegrees);
			    			$("#fahrenheit").html(data.result.fahrenheitDegrees);
			    			$("#sunrise").html(data.result.sunriseTime);
			    			$("#sunset").html(data.result.sunsetTime);
			    			$("#weatherDescription").html(data.result.overallDescription);
				    	}else{
				    		alert("NO RESULTS FOUND");	
					    }
			        },
			    	error:   function (xhr, ajaxOptons, thrownError) {
			    		alert("AN ERROR OCCURRED WHILE CALLING THE WS")
			        }
			    });
		  }else{
			  alert("NOTHING TO SHOW");
		  }		  
	    
	  });
	});
</script>
</head>
<body>
	<h2>Weather System</h2>
	<select id="city">
	  <option value="">Select a City</option>
	  <option value="London">London</option>
	  <option value="Hong Kong">Hong Kong</option>
	</select>
	<br/>
	<label id="cityName"></label>
	<br/>
	TODAY IS <label id="todayDate"></label>
	<br/>
	<label id="celsius"></label>�C
	<br/>
	<label id="fahrenheit"></label>�F
	<br/>
	Sunrise Time: <label id="sunrise"></label>
	<br/>
	Sunset Time: <label id="sunset"></label>
	<br/>
	<label id="weatherDescription"></label>
</body>
</html>
