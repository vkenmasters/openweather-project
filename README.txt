**** WEATHER SYSTEM ****

HOW TO INSTALL

- Using a console or terminal, navigate to the "openweather-system" project

- Using Maven, run the following command:

	mvn jetty:run
	
When the message "Started Jetty Service" appears, go to a browser and enter the following url:

	http://localhost:8080/weather-system/index.jsp
	


TODO LIST

- Add styles to the jsp page
- Missing CityWeatherRest Test Cases



